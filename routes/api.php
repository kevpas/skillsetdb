<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


//Route::middleware('auth:api')->get('/projects', 'ProjectController@index');


Route::middleware('auth:api')->group(function () {
    Route::resource('/profiles', 'ProfileController');
    Route::resource('/projects', 'ProjectController');
    Route::resource('/skills', 'SkillController');
    Route::post('/profile/syncskill', 'ProfileController@syncSkill');
    Route::resource('/users', 'UserController');
    Route::get('/userinfo', 'UserController@userInfo');
});
