<?php
namespace App\Traits;

use Ramsey\Uuid\Uuid;

trait Uuids
{
    /**
     * Boot function from laravel.
     */
    protected static function bootUuids()
    {

        static::creating(function ($model) {
            $model->incrementing = false;
            $model->{$model->getKeyName()} = Uuid::uuid4()->toString();
        });

        static::saving(function ($model) {
            $original_uuid = $model->getOriginal('id');

            if ($original_uuid !== $model->{$model->getKeyName()}) {
                $model->{$model->getKeyName()} = $original_uuid;
            }
        });
    }
}