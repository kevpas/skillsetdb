<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Profile;
use App\User;
use Auth;

class ProfileController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Profile::with('user')->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($profile_name)
    {
        // get the profile
        $profile = Profile::with('user')->with('skills')->with('projects')->where('profile_name', $profile_name)->firstOrFail();

        // return the profile if authorization passed
        return $profile;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // get the profile
        $profile = Profile::find($id);

        // See if the user may edit the profile
        $this->authorize('edit',$profile);

        // Perform the edit

        // return something

    }

    public function syncSkill(Request $request){

        $this->validate($request, ['skill_id' => 'required', 'progress' => 'required']);

        // get the profile
        $user = $request->user();
        $profile = $user->profile;

        // See if the user may edit the profile

        // Perform the skill sync
        return $profile->skills()->sync( [request('skill_id') => ['progress' => request('progress')] ], false );

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
