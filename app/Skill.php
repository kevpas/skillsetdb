<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Skill extends Model
{
    use Traits\Uuids;

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    protected $fillable = [
        'name'
    ];

    public function employees()
    {
        return $this->belongsToMany(Profile::class);
    }

}
