<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    use Traits\Uuids;

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    public function members()
    {
        return $this->belongsToMany(Profile::class)->select(['name', 'email']);
    }

}
