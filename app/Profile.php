<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    use  Traits\Uuids;

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    protected $hidden = [
        'user_id'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function skills()
    {
        return $this->belongsToMany(Skill::class)->withPivot('progress');
    }

    public function projects()
    {
        return $this->belongsToMany(Project::class);
    }
}
