<md-list>
    <md-list-item>
        <router-link @click.native="$refs.sidebar.toggle()" to="/" exact>Home</router-link>
    </md-list-item>
    <md-list-item>
        <router-link @click.native="$refs.sidebar.toggle()" to="/projects">Projects</router-link>
    </md-list-item>
    <md-list-item>
        <router-link @click.native="$refs.sidebar.toggle()" to="/employees">Employees</router-link>
    </md-list-item>
    <md-list-item>
        <router-link @click.native="$refs.sidebar.toggle()" to="/skills">Skills</router-link>
    </md-list-item>
    <md-list-item>
        <router-link @click.native="$refs.sidebar.toggle()" to="/users">Users</router-link>
    </md-list-item>
</md-list>