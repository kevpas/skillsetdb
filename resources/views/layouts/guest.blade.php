<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="/css/app.css">
    <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Roboto:300,400,500,700,400italic">
    <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Exo">
    <link rel="stylesheet" href="//fonts.googleapis.com/icon?family=Material+Icons">
    <title>SkillsetDB</title>

</head>
<body id="guest">
<div class="container ">

    <div class="header">
        Skillset Database
    </div>

    <div class="main-content">
        <div class="page-wrapper">
            @yield('content')
        </div>
    </div>

</div>


</body>

</html>
