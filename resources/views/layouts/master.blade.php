<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" type="text/css" href="/css/app.css">
    <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Roboto:300,400,500,700,400italic">
    <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Exo">
    <link rel="stylesheet" href="//fonts.googleapis.com/icon?family=Material+Icons">

    <script href="https://unpkg.com/vue-material@0.7.5/dist/vue-material.js"></script>

    <title>SkillsetDB</title>

</head>
<body>
<div id="app" class="container">
    <md-sidenav class="md-left" ref="sidebar">
        <md-toolbar class="md-large">
            <div class="md-toolbar-container">
                <div class="md-title app-title">Skillset Database</div>

            </div>

            <div class="md-toolbar-container logo">
                <img src="/img/logo.png"/>
            </div>

            <div class="md-toolbar-container">

                <div class="md-title" style="flex:1;margin-left:8px">{{ Auth::user()->name }}</div>

                <md-menu md-direction="bottom left">
                    <md-button md-menu-trigger class="md-icon-button">
                        <md-icon >keyboard_arrow_down</md-icon>
                    </md-button>
                    <md-menu-content>
                        <md-menu-item href="#/employees/{{ Auth::user()->profile->profile_name }}">
                            My Profile
                        </md-menu-item>
                        <md-menu-item class="md-icon-button" href="{{ route('logout') }}"
                                      onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                            Logout
                        </md-menu-item>
                        <!-- Disabled is an option for md-menu-items -->
                    </md-menu-content>
                </md-menu>
            </div>


        </md-toolbar>

        @include('layouts.nav');

    </md-sidenav>

    <notifications position="top center"></notifications>

    <div class="main-content">

        <md-toolbar class="page-header">
            <md-button class="md-icon-button menu" @click="$refs.sidebar.toggle()">
                <md-icon>menu</md-icon>
            </md-button>
            <h2 class="md-title" style="flex: 1">@{{ $route.name }}</h2>
        </md-toolbar>

        <div class="page-wrapper">
            <transition name="slideup">
                <router-view></router-view>
            </transition>
        </div>

    </div>

</div>
<script src="/js/app.js"></script>


<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
    {{ csrf_field() }}
</form>
</body>
</html>
