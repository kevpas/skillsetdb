import Vue from 'vue';
import VueRouter from 'vue-router';
import axios from 'axios';
import VueMaterial from 'vue-material';
import Form from './utilities/Form';
import Notifications from 'vue-notification';
import Lodash from 'lodash';


window.Vue = Vue;
Vue.use(VueRouter);
Vue.use(VueMaterial);
Vue.use(Notifications);
Vue.use(Lodash);


window.axios = axios;
window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

let token = document.head.querySelector('meta[name="csrf-token"]');
if (token) {
    window.axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;
} else {
    console.error('CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token');
}

window.Form = Form;