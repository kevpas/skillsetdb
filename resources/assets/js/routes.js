import VueRouter from 'vue-router';

let routes = [
    {
        path: '/',
        name: 'Home',
        component: require('./views/home')
    },
    {
        path: '/projects',
        name: 'Projects',
        component: require('./views/projects')
    },
    {
        path: '/projects/new',
        name: 'New Project',
        component: require('./views/project-new')
    },
    {
        path: '/projects/:id',
        name: 'Project details',
        component: require('./views/project')
    },

    {
        path: '/employees',
        name: 'Employees',
        component: require('./views/employees')
    },
    {
        path: '/employees/:id',
        name: 'Employee details',
        component: require('./views/employee')
    },
    {
        path: '/skills',
        name: 'Skills',
        component: require('./views/skills')
    },
    {
        path: '/skills/:id',
        name: 'Skill details',
        component: require('./views/skill')
    },
    {
        path: '/users',
        name: 'Users',
        component: require('./views/users')
    },
    {
        path: '/users/:id',
        name: 'User details',
        component: require('./views/user')
    }
];

export default new VueRouter({
    routes,
    linkActiveClass: 'is-active'
});