import './bootstrap';
import router from './routes';
import 'vue-material/dist/vue-material.css';
import Store from './store';

Vue.component('skill-progress', require('./components/skill-progress.vue'));
Vue.component('project-block', require('./components/project-block.vue'));
Vue.component('passport-clients', require('./components/passport/Clients.vue'));
Vue.component('passport-authorized-clients', require('./components/passport/AuthorizedClients.vue'));
Vue.component('passport-personal-access-tokens', require('./components/passport/PersonalAccessTokens.vue'));

const app = new Vue({
    el: '#app',

    router,
    data(){
        return {
            $store: Store
        }
    },

    created(){
        axios.get("/api/userinfo").then(response => {
            console.log(response.data);
            this.$root.$data.$store.name = response.data.name;
            this.$root.$data.$store.profile_page = response.data.profile.profile_name;
        })
    }
});


Vue.material.registerTheme('default', {
    primary: {
        color: 'red',
        hue: 400
    },
    accent: {
        color: 'blue-grey',
        hue: 600
    },
    background: {
        color: 'grey',
        hue: 100
    }
});